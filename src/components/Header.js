import React from 'react';

function Header(props) {
  const isLoggedIn = props.isLoggedIn;
  if (isLoggedIn) {
    return (
      <div className='header'>
        <div className='header-elem'>Home</div>
        <div className='header-elem'>Test</div>
        <div className='header-elem'>Test</div>
      </div>
    );
  } else {
    return (
      <div className='header'>
        <div className='header-elem'>Home</div>
      </div>
    );
  }
}

export default Header;
