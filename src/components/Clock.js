import React, { Component } from 'react';

export class Clock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date().toLocaleDateString(),
      number: 0,
      show: false,
    };
  }

  handleClick = (event) => {
    // this.setState({ number: this.state.number + 1 });      -> Considered wrong https://reactjs.org/docs/faq-state.html#what-is-the-difference-between-state-and-props
    this.setState((state) => ({ number: state.number + 1 }));
    console.log(event);
  };
  showDiv = () => {
    this.setState({ show: !this.state.show });
  };
  render() {
    return (
      <div>
        <div>{this.state.date}</div>
        <div>{this.state.number}</div>
        {this.state.show && <div>Hidden text</div>}
        <button onClick={(e) => this.handleClick(e)}>Increase</button>
        <button onClick={this.showDiv}>Show Content</button>
      </div>
    );
  }
}

export default Clock;
