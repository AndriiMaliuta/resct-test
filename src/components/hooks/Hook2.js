import React, { useState, useEffect } from 'react';

function Hook2(props) {
  const [name, setName] = useState('');

  useEffect(() => {
    localStorage.setItem('name', 'Vasyl');
  });

  function getNameFromStorage() {
    return localStorage.getItem('name');
  }

  function setNameFromStorage() {
    setName(getNameFromStorage);
  }

  //   function handleSubmit(e) {
  //     e.preventDefault();
  //   }

  function handleChange(e) {
    e.preventDefault();
    // let name = document.querySelector('#name').value;

    setName(e.target.value);
  }

  return (
    <div className='my-div'>
      <p>Your name is {name}</p>
      <form onSubmit={handleChange}>
        <input
          onChange={handleChange}
          value={name}
          id='name'
          name='name'
          type='text'
        />
        <button onChange={handleChange} className='button'>
          Change
        </button>
      </form>
    </div>
  );
}

export default Hook2;
