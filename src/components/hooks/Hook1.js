import React, { useState, useEffect } from 'react';

function Hook1() {
  const [count, setCount] = useState(0);
  useEffect(() => {
    document.title = `You clicked ${count} times`;
  });
  return (
    <div className='my-div'>
      <p>You clicked {count} times</p>
      <button className='button' onClick={() => setCount(count + 1)}>
        Click
      </button>
    </div>
  );
}

export default Hook1;
