import React from 'react';

const Footer = (props) => {
  return (
    <div className='footer'>
      <p>Some footer here and custom {props.value} value</p>
    </div>
  );
};

export default Footer;
