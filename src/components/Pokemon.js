import React, { Component } from 'react';
import axios from 'axios';

export class Pokemon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pokemons: {},
      config: {
        type: 'submit',
        className: 'button',
      },
    };
  }

  handleTestBtnClick = (e) => {
    console.log(e.target.dataset.name);
  };

  componentDidMount() {
    console.log(this.props);
    axios
      .get(`https://pokeapi.co/api/v2/pokemon`)
      .then((response) =>
        this.setState((state) => ({ pokemons: response.data }))
      )
      .catch((error) => console.log(error));
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log(prevProps, prevState, snapshot);
  }

  render() {
    return (
      <React.Fragment>
        <div className='my-div'>
          <h3>Pokemons</h3>
          <div>Pokemons Count == {this.state.pokemons.count}</div>
          <div>
            <p>Children == {this.props.children}</p>
            {/* <ol>
              {this.state.pokemons.results.map((poke) => (
                <li key={poke.name}>{poke.name}</li>
              ))}
            </ol> */}
            <p>Default Prop Age == {this.props.age}</p>
            <button
              className='button'
              data-name='Some Name in data'
              onClick={this.handleTestBtnClick}
              {...this.state.config}
            >
              TEST
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

Pokemon.defaultProps = {
  age: 8,
};

export default Pokemon;
