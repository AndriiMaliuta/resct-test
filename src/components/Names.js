import React from 'react';

function Names(props) {
  const names = props.names;
  const listItems = names.map((name, index) => (
    <div key={index} className='name'>
      <p>{index}</p>
      <p>{name}</p>
    </div>
  ));
  return <ul>{listItems}</ul>;
}

export default Names;
