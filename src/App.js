import React from 'react';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Pokemon from './components/Pokemon';
import Clock from './components/Clock';
import Names from './components/Names';
import Hook1 from './components/hooks/Hook1';
import Hook2 from './components/hooks/Hook2';

function App() {
  return (
    <div className='App'>
      <Header isLoggedIn={false} />
      <Pokemon />
      {/* <Clock /> */}
      {/* <Names names={['Pikachu', 'Ditto', 'Bulbozaur']} /> */}
      <Hook1 />
      <Hook2 />
      <Footer value={'TEST'} />
    </div>
  );
}

export default App;
